# Custom email templates

Nhost plans to fully host custom email templates for email verification, password reset, email change confirmation, passwordless authentication...

In the meantime, it is possible to tell the Hasura-auth service to look for templates in an external public location through HTTP requests.
Here is a short example on how to proceed.

## Define where to find the custom templates

In the Nhost console, define the following **App Environment Variable:**:

- Variable name: `AUTH_EMAIL_TEMPLATE_FETCH_URL`
- value: ``
